<!DOCTYPE html>

<html>

<head>

    <title>Drzewo tematów</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link href="{{ asset('css/treeview.css') }}" rel="stylesheet">

</head>

<body>

<div class="container">

    <div class="panel panel-primary">

        <div class="panel-heading">Mechanizm zarządzania strukturą drzewiastą -</div>

        <div class="panel-body">

            <div class="row">

                <div class="col-md-6">

                    <h3>Lista tematów</h3>

                    <ul id="tree1">

                        @foreach($categories as $category)

                            <li> 
                               {{ $category->title }} <a href="{!! route("usunkategorie", ['id' => $category->id]) !!}" style="color:red">Usuń </a>  
                                    
                                @if(count($category->childs))

                                    @include('category.manageChild',['childs' => $category->childs])

                                @endif

                            </li>

                        @endforeach

                    </ul>

                </div>

                <div class="col-md-6">

                    <h3>Dodawanie nowej kategorii</h3>

                    <form role="form" id="category" method="POST" action="{{ route('add.category') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">

                        <label>Nazwa:</label>

                        <input type="text" id="title" name="title" value="" class="form-control" placeholder="Nazwa elementu">
                        @if ($errors->has('title'))
                            <span class="text-red" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif

                    </div>


                    <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">

                        <label>Folder:</label>
                        <select id="parent_id" name="parent_id" class="form-control">
                            <option value="0">Wybierz do czego podpiąć</option>
                            @foreach($allCategories as $rows)
                                    <option value="{{ $rows->id }}">{{ $rows->title }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('parent_id'))
                            <span class="text-red" role="alert">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </span>
                        @endif

                    </div>

                    
                    <div class="form-group">

                        <button type="submit" class="btn btn-success">Dodaj nowy element drzewa</button>

                    </div>
                </form>


<form role="form" id="transportcategory" method="POST" action="{{ route('przenieskategorie') }}" enctype="multipart/form-data">
    @csrf         <div>     <label>Co chcesz przerzucić:</label>
                            <select id="coPrzeniesc" name="coPrzeniesc" class="form-control">
                            
                                @foreach($allCategories as $rows)
                                        <option value="{{ $rows->id }}">{{ $rows->title }}</option>
                                @endforeach
                            </select>

                            
                            <label>Dokąd chcesz przerzucić:</label>
                            <select id="gdziePrzeniesc" name="gdziePrzeniesc" class="form-control">
                                <option value="0">Rodzic</option>
                                @foreach($allCategories as $rows)
                                        <option value="{{ $rows->id }}">{{ $rows->title }}</option>
                                @endforeach
                            </select>

                                <br>
    
 
                    <div class="form-group">

                        <button type="submit" class="btn btn-success">Przerzuć!</button>

                    </div>
                </form>





                <form role="form" id="przemianujcategory" method="POST" action="{{ route('przemianujcategory') }}" enctype="multipart/form-data">
                    @csrf         <div>     <label>Wybierz rzecz do przemianowania:</label>
                                            <select id="przemianuj" name="przemianuj" class="form-control">
                                            
                                                @foreach($allCategories as $rows)
                                                        <option value="{{ $rows->id }}">{{ $rows->title }}</option>
                                                @endforeach
                                            </select>
                
                                            
                                            <label>Wprowadź nową nazwę:</label>
                                            
                                            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">

                                                <input type="text" id="nowytytul" name="nowytytul" value="" class="form-control" placeholder="Nowa nazwa dla pliku">
                                                @if ($errors->has('title'))
                                                    <span class="text-red" role="alert">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif   </div>
                        


                                    <div class="form-group">
                
                                        <button type="submit" class="btn btn-success">Przemianuj!</button>
                
                                    </div>
                                </form>
                
                




                    <div class="form-group">

      {{-- sortowanie wszystkiego a-z --}}


                        <a href="{{ route('category-tree-view', ['sort'=>'az']) }}" type="submit" class="btn btn-success">Sortuj A-Z!</a>

                    </div>







                    <div class="form-group">


                            {{-- sortowanie wszystkiego Z-A --}}

                      
                            <a href="{{ route('category-tree-view', ['sort'=>'za']) }}" type="submit" class="btn btn-success">Sortuj Z-A!</a>


                    </div>








                    <div class="form-group">


                  
                  
                        <a href="{{ route('category-tree-view')}}" type="submit" class="btn btn-success">Przywróć niesortowane!</a>


                </div>









         </div>









                   


                </div>

            </div>




        </div>

    </div>

</div>

<script src="{{ asset('js/treeview.js') }}"></script>

</body>

</html>