<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function manageCategory(Request $request)
    {
        $sortType = $request->get('sort');

        if($sortType ==='az'){
            $categories = Category::where('parent_id', '=', 0)->orderBy('title', 'ASC')->get();
            $allCategories = Category::orderBy('title', 'ASC')->get();
        }
        else if($sortType ==='za'){
            $categories = Category::where('parent_id', '=', 0)->orderBy('title', 'desc')->get();
            $allCategories = Category::orderBy('title', 'desc')->get();
        }
        else{
            $categories = Category::where('parent_id', '=', 0)->get();
            $allCategories = Category::all();
        }
        foreach($categories as $category){
            $category->sortChildren=$sortType;
        }
        foreach($allCategories as $category ){
            $category->sortChildren=$sortType;
        }
        return view('category.categoryTreeview',compact('categories','allCategories'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function addCategory(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];

        Category::create($input);
        return back()->with('success', 'New Category added successfully.');

    }



    public function removeCategory($id)  
        {
            $category = Category::find($id); 
     
            $category->destroyWithChildren(); 

        
            return redirect()->route('category-tree-view');
        }





        public function transportCategory(Request $request)  
        {
        
            $id= $request->input('coPrzeniesc');
            $target_id= $request->input('gdziePrzeniesc');
            $category = Category::find($id);  //znajdujemy kategorie
           
            $category->parent_id=$target_id;
            $category->save();
          
            $categories = Category::where('parent_id', '=', 0)->get();

            $allCategories = Category::all();
    

            return back()->with('success', 'Udało się zmienić przenieść!.');
        }







        public function editCategory($id)  
        {
         
        
            return view('category.categoryTreeview',compact('categories','allCategories'));
        }








        public function sortAZCategory($id)  
        {

            $c = collect($c);
            $sorted = $c->sortBy('title');
            $c = $c->all();
            
            return view('category.categoryTreeview',compact('categories','allCategories'));
        }



        public function sortZACategory($id)  
        {
            return view('category.categoryTreeview',compact('categories','allCategories'));
        }



        
        public function przemianujCategory(Request $request)  
        {
        
            $this->validate($request, [
                'nowytytul' => 'required',
            ]);
           
            $input = $request->all();
            $id = $request->input('przemianuj');

                
            $category = Category::where('id', '=', $id)->first();
                $category->title=$request->input('nowytytul');
                $category->save();
            return back()->with('success', 'Udało się zmienić nazwę!.');
            
        }




}
