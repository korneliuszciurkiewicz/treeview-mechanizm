<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;

class Category extends Model
{
      public $fillable = ['title','parent_id'];
    public $sortChildren;
    /**

     * Get the index name for the model.

     *

     * @return string

     */


    public function destroyWithChildren(){
        $childrenIds = $this->getChildrenTrees(); //wywołuje gunkcję getChirdrelTrees() 1

        foreach($childrenIds as $id){
            $childCategory = \App\Category::find($id);   
            $childCategory->delete();   //usuwa 
        }
    }
    public function getChildrenTrees(){  //funkcja wyszukuje drzewo z dziecmi i wywoluje funkcje getChildren ze zmienną parent (inny parent = inne dzieci, oczywiste)
        $parent = $this;
        $array_of_ids = $this->getChildren($parent);
        array_push($array_of_ids,$this->id);
        return $array_of_ids;        
    }
    
    private function getChildren($category){ //pobiera dzieci jako categorie
        $ids = [];
        foreach ($category->childs as $cat) { 
            $ids[] = $cat->id;
            $ids = array_merge($ids, $this->getChildren($cat));  //merguje dzieci i wysyla je do usuniecia
        }
        return $ids;
    }
    public function childs() {
        if($this->sortChildren ==='az'){
            return $this->hasMany('App\Category','parent_id','id')->orderBy('title', 'ASC') ;
        }
        else if($this->sortChildren ==='za'){
            return $this->hasMany('App\Category','parent_id','id')->orderBy('title', 'DESC') ;
        }
        else {
            return $this->hasMany('App\Category','parent_id','id') ;
        }
    }
}
